$(function () {

    if (BX && BX.PopupWindowManager && BX.PopupWindowManager.create) {
        BX.PopupWindowManager.createCustom = BX.PopupWindowManager.create;
        BX.PopupWindowManager.create = function (uniquePopupId, bindElement, params) {
            var popupWindow = BX.PopupWindowManager.createCustom(uniquePopupId, bindElement, params);
            $('#' + popupWindow.uniquePopupId).addClass('popup-window-container');
            return popupWindow;
        };
    }

});
